package com.cloudsherpas.payroll.models;


public class EmployeeSalaryDeductionModel {

    private double incomeTaxDeduction;
    private double sssDeduction;
    private double philHealthDeduction;
    private double pagibigDeduction;
    private double absencesDeduction;

    public double getIncomeTaxDeduction() {
        return incomeTaxDeduction;
    }
    public void setIncomeTaxDeduction(final double incomeTaxDeduction) {
        this.incomeTaxDeduction = incomeTaxDeduction;
    }
    public double getSssDeduction() {
        return sssDeduction;
    }
    public void setSssDeduction(final double sssDeduction) {
        this.sssDeduction = sssDeduction;
    }
    public double getPhilHealthDeduction() {
        return philHealthDeduction;
    }
    public void setPhilHealthDeduction(final double philHealthDeduction) {
        this.philHealthDeduction = philHealthDeduction;
    }
    public double getPagibigDeduction() {
        return pagibigDeduction;
    }
    public void setPagibigDeduction(final double pagibigDeduction) {
        this.pagibigDeduction = pagibigDeduction;
    }
    public double getAbsencesDeduction() {
        return absencesDeduction;
    }
    public void setAbsencesDeduction(final double absencesDeduction) {
        this.absencesDeduction = absencesDeduction;
    }

}
