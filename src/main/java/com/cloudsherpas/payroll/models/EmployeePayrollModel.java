package com.cloudsherpas.payroll.models;

import java.util.Date;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
@Index
public class EmployeePayrollModel {

    @Id
    private Long id;
    private String employeeKey;
    private double dailyRate;
    private double taxRate;
    private double overtimePay;
    private double grossSalary;
    private double netSalary;
    private EmployeeSalaryDeductionModel deductions;
    private AllowancesModel allowances;

    private Date dateTo;
    private Date dateFrom;

    public Long getId() {
        return id;
    }
    public void setId(final Long id) {
        this.id = id;
    }
    public String getEmployeeKey() {
        return employeeKey;
    }
    public void setEmployeeKey(final String employeeKey) {
        this.employeeKey = employeeKey;
    }
    public double getDailyRate() {
        return dailyRate;
    }
    public void setDailyRate(final double dailyRate) {
        this.dailyRate = dailyRate;
    }
    public double getTaxRate() {
        return taxRate;
    }
    public void setTaxRate(final double taxRate) {
        this.taxRate = taxRate;
    }
    public double getOvertimePay() {
        return overtimePay;
    }
    public void setOvertimePay(final double overtimePay) {
        this.overtimePay = overtimePay;
    }
    public double getGrossSalary() {
        return grossSalary;
    }
    public void setGrossSalary(final double grossSalary) {
        this.grossSalary = grossSalary;
    }
    public double getNetSalary() {
        return netSalary;
    }
    public void setNetSalary(final double netSalary) {
        this.netSalary = netSalary;
    }
    public EmployeeSalaryDeductionModel getDeductions() {
        return deductions;
    }
    public void setDeductions(final EmployeeSalaryDeductionModel deductions) {
        this.deductions = deductions;
    }
    public Date getDateTo() {
        return dateTo;
    }
    public void setDateTo(final Date dateTo) {
        this.dateTo = dateTo;
    }
    public Date getDateFrom() {
        return dateFrom;
    }
    public void setDateFrom(final Date dateFrom) {
        this.dateFrom = dateFrom;
    }
    public AllowancesModel getAllowances() {
        return allowances;
    }
    public void setAllowances(final AllowancesModel allowances) {
        this.allowances = allowances;
    }
}
