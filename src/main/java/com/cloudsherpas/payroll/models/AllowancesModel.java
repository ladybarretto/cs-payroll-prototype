package com.cloudsherpas.payroll.models;

public class AllowancesModel {
    private double gasAllowance;
    private double communicationAllowance;
    private double riceAllowance;
    private double clothingAllowance;
    public double getGasAllowance() {
        return gasAllowance;
    }
    public void setGasAllowance(final double gasAllowance) {
        this.gasAllowance = gasAllowance;
    }
    public double getCommunicationAllowance() {
        return communicationAllowance;
    }
    public void setCommunicationAllowance(final double communicationAllowance) {
        this.communicationAllowance = communicationAllowance;
    }
    public double getRiceAllowance() {
        return riceAllowance;
    }
    public void setRiceAllowance(final double riceAllowance) {
        this.riceAllowance = riceAllowance;
    }
    public double getClothingAllowance() {
        return clothingAllowance;
    }
    public void setClothingAllowance(final double clothingAllowance) {
        this.clothingAllowance = clothingAllowance;
    }
}
