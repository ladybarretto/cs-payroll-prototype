package com.cloudsherpas.payroll.constants;

public final class ServiceConstants {

    public static final int SIXTY_MINUTES = 60;
    private ServiceConstants() {

    }
}
