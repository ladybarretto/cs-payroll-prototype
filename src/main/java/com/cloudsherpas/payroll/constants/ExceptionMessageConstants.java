package com.cloudsherpas.payroll.constants;

public final class ExceptionMessageConstants {
    public static final String NO_CONTENTS_FOUND = "No records found";
    public static final String PERSIST_FAILED = "Cannot persist data";
    public static final String NULL_EMPLOYMENT_DETAILS_FOUND = "No employment details found";
    public static final String NULL_DATE = "Date String is required";
    public static final String NULL_COUNTRYCODE = "Country code is required";

    private ExceptionMessageConstants() {
    }
}
