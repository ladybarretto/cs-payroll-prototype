package com.cloudsherpas.payroll.exception;

import com.google.api.server.spi.ServiceException;

//CSOFF: MagicNumber
public class PayrollModuleException extends ServiceException {

    public PayrollModuleException(final String statusMessage) {
        super(408, statusMessage);
    }
}
//CSON: MagicNumber

