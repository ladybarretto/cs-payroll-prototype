package com.cloudsherpas.payroll.util;

import java.io.IOException;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.google.appengine.repackaged.org.codehaus.jackson.JsonParser;
import com.google.appengine.repackaged.org.codehaus.jackson.map.DeserializationContext;
import com.google.appengine.repackaged.org.codehaus.jackson.map.JsonDeserializer;

public class JodaDateTimeDeserializer extends JsonDeserializer<DateTime> {

    private static final DateTimeFormatter FORMATTER = DateTimeFormat
            .forPattern("dd-MM-yyyy");

    @Override
    public DateTime deserialize(final JsonParser jsonParser,
            final DeserializationContext deserializationContext) throws IOException {
        return FORMATTER.parseDateTime(jsonParser.getText());
    }
}
