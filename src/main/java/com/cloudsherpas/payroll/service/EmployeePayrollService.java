package com.cloudsherpas.payroll.service;

import java.util.List;

import com.cloudsherpas.payroll.dto.core.EmployeeDetailsDTO;
import com.cloudsherpas.payroll.dto.core.EmployeePayrollDetailsDTO;
import com.cloudsherpas.payroll.exception.PayrollModuleException;

public interface EmployeePayrollService {


    EmployeePayrollDetailsDTO computeSalaryforEmployee(final EmployeeDetailsDTO employeeDTO) throws PayrollModuleException;

    List<EmployeePayrollDetailsDTO> getAllSalary(final String key) throws PayrollModuleException;
}
