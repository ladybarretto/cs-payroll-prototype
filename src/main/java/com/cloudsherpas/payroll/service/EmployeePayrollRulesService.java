package com.cloudsherpas.payroll.service;

import com.cloudsherpas.payroll.dto.core.EmployeeDetailsDTO;
import com.cloudsherpas.payroll.dto.core.EmployeePayrollDetailsDTO;
import com.cloudsherpas.payroll.exception.PayrollModuleException;

public interface EmployeePayrollRulesService {

    EmployeePayrollDetailsDTO getEmployeePayrollDetails(final EmployeeDetailsDTO employeeDetails) throws PayrollModuleException;
    EmployeePayrollDetailsDTO getEmployeeCompensationAndBenefit(final EmployeeDetailsDTO employeeDetails);
}

