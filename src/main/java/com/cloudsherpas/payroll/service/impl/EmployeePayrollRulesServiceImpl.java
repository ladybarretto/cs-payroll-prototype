package com.cloudsherpas.payroll.service.impl;

import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.DecisionTableConfiguration;
import org.drools.builder.DecisionTableInputType;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.io.ResourceFactory;
import org.drools.runtime.StatefulKnowledgeSession;

import com.cloudsherpas.payroll.constants.ExceptionMessageConstants;
import com.cloudsherpas.payroll.dto.core.EmployeeDetailsDTO;
import com.cloudsherpas.payroll.dto.core.EmployeePayrollDeductionsDTO;
import com.cloudsherpas.payroll.dto.core.EmployeePayrollDetailsDTO;
import com.cloudsherpas.payroll.dto.core.EmploymentDetailsDTO;
import com.cloudsherpas.payroll.exception.PayrollModuleException;
import com.cloudsherpas.payroll.service.EmployeePayrollRulesService;

public class EmployeePayrollRulesServiceImpl implements
        EmployeePayrollRulesService {
    private static final String EMP_PAYROLL_DETAILS = "empPayrollDetails";

    private StatefulKnowledgeSession session;

    @Override
    public EmployeePayrollDetailsDTO getEmployeePayrollDetails(
            final EmployeeDetailsDTO employeeDetails)
            throws PayrollModuleException {

        KnowledgeBase knowledgeBase = createKnowledgeBaseFromXLS(
                employeeDetails.getCountryCode(), "Compensation");
        session = knowledgeBase.newStatefulKnowledgeSession();

        if (null == employeeDetails.getEmploymentDetails()) {
            throw new PayrollModuleException(
                    ExceptionMessageConstants.NULL_EMPLOYMENT_DETAILS_FOUND);
        }
        EmploymentDetailsDTO employmentDetails = employeeDetails
                .getEmploymentDetails();
        EmployeePayrollDetailsDTO empPayrollDetails = new EmployeePayrollDetailsDTO();
        empPayrollDetails.setDeductions(new EmployeePayrollDeductionsDTO());
        session.insert(employeeDetails);
        session.insert(employmentDetails);
        session.insert(empPayrollDetails);
        session.setGlobal(EMP_PAYROLL_DETAILS, empPayrollDetails);
        session.fireAllRules();
        return empPayrollDetails;

    }

    @Override
    public EmployeePayrollDetailsDTO getEmployeeCompensationAndBenefit(
            final EmployeeDetailsDTO employeeDetails) {
        KnowledgeBase knowledgeBase = createKnowledgeBaseFromXLS(
                employeeDetails.getCountryCode(), "Compensation");
        session = knowledgeBase.newStatefulKnowledgeSession();
        EmploymentDetailsDTO employmentDetails = employeeDetails
                .getEmploymentDetails();
        EmployeePayrollDetailsDTO empPayrollDetails = new EmployeePayrollDetailsDTO();
        session.insert(employeeDetails);
        session.insert(empPayrollDetails);
        session.setGlobal(EMP_PAYROLL_DETAILS, empPayrollDetails);
        session.fireAllRules();
        return empPayrollDetails;

    }

    private KnowledgeBase createKnowledgeBaseFromXLS(final String countryCode,
            final String sheetName) {
        DecisionTableConfiguration dtconf = KnowledgeBuilderFactory
                .newDecisionTableConfiguration();
        dtconf.setInputType(DecisionTableInputType.XLS);
        dtconf.setWorksheetName(sheetName);
        KnowledgeBuilder knowledgeBuilder = KnowledgeBuilderFactory
                .newKnowledgeBuilder();
        knowledgeBuilder.add(ResourceFactory
                .newClassPathResource("EMP_COMPENSATION_BENEFITS_RULES_"
                        + countryCode + ".xls"), ResourceType.DTABLE, dtconf);
        if (knowledgeBuilder.hasErrors()) {
            throw new RuntimeException(knowledgeBuilder.getErrors().toString());
        }

        KnowledgeBase knowledgeBase = KnowledgeBaseFactory.newKnowledgeBase();
        knowledgeBase.addKnowledgePackages(knowledgeBuilder
                .getKnowledgePackages());
        return knowledgeBase;

    }
}
