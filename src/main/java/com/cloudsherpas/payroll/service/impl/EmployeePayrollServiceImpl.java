package com.cloudsherpas.payroll.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import org.drools.core.util.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.util.CollectionUtils;

import com.cloudsherpas.payroll.constants.ExceptionMessageConstants;
import com.cloudsherpas.payroll.constants.ServiceConstants;
import com.cloudsherpas.payroll.dao.EmployeePayrollDAO;
import com.cloudsherpas.payroll.dto.core.EmployeeDetailsDTO;
import com.cloudsherpas.payroll.dto.core.EmployeePayrollDeductionsDTO;
import com.cloudsherpas.payroll.dto.core.EmployeePayrollDetailsDTO;
import com.cloudsherpas.payroll.dto.core.EmployeeTimeAndAttendanceDTO;
import com.cloudsherpas.payroll.dto.core.EmploymentDetailsDTO;
import com.cloudsherpas.payroll.dto.core.HolidayPayRulesDTO;
import com.cloudsherpas.payroll.dto.core.OvertimeRulesDTO;
import com.cloudsherpas.payroll.enums.HolidayTypeEnum;
import com.cloudsherpas.payroll.enums.OvertimeTypeEnum;
import com.cloudsherpas.payroll.exception.PayrollModuleException;
import com.cloudsherpas.payroll.models.EmployeePayrollModel;
import com.cloudsherpas.payroll.service.EmployeePayrollRulesService;
import com.cloudsherpas.payroll.service.EmployeePayrollService;
import com.google.appengine.repackaged.com.google.api.client.util.Lists;

public class EmployeePayrollServiceImpl implements EmployeePayrollService {

    @Autowired
    @Qualifier("employeePayrollRulesService")
    @Lazy
    private EmployeePayrollRulesService employeePayrollRulesService;

    @Autowired
    @Qualifier("employeePayrollDAO")
    @Lazy
    private EmployeePayrollDAO employeePayrollDAO;

    private ModelMapper modelMapper;

    public EmployeePayrollServiceImpl() {
        modelMapper = new ModelMapper();
    }

    @Override
    public EmployeePayrollDetailsDTO computeSalaryforEmployee(
            final EmployeeDetailsDTO employeeDetails) throws PayrollModuleException {
        final double dailyMinimumHours = employeeDetails
                .getWorkingTimeRules().getDailyMinimumWorkHours();
        final Map<HolidayTypeEnum, HolidayPayRulesDTO> holidayRules = employeeDetails.getHolidayPayRules();
        final Map<OvertimeTypeEnum, OvertimeRulesDTO> overtimeRules = employeeDetails.getOvertimeRules();
        final EmploymentDetailsDTO employmentDetails = employeeDetails.getEmploymentDetails();

        EmployeePayrollDetailsDTO employeePayrollDetails = new EmployeePayrollDetailsDTO();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");

        employeePayrollDetails = employeePayrollRulesService
                .getEmployeePayrollDetails(employeeDetails);
        double overtimePay = 0d;
        double holidayPay = 0d;
        double absencesDeduction = 0d;
        double tardinessDeduction = 0d;
        double undertimeDeduction = 0d;

        for (final EmployeeTimeAndAttendanceDTO attendance : employeeDetails.getTimeAndAttendanceList()) {
            overtimePay += computeOvertimePay(dailyMinimumHours, overtimeRules, employmentDetails, attendance);
            holidayPay += computeHolidayPay(dailyMinimumHours, holidayRules, employmentDetails, attendance);
            absencesDeduction += computeAbsencesDeduction(employmentDetails, attendance);
            tardinessDeduction += computeTardinessDeduction(attendance.getTardiness(), employmentDetails);
            undertimeDeduction += computeUndertimeDeduction(attendance.getUndertime(), employmentDetails);
        }
        employeePayrollDetails.setTotalHolidayPay(holidayPay);
        employeePayrollDetails.setTotalOvertimePay(overtimePay);

        EmployeePayrollDeductionsDTO deductions = employeePayrollDetails.getDeductions();
        if (deductions == null) {
            deductions = new EmployeePayrollDeductionsDTO();
        }
        deductions.setAbsencesDeduction(absencesDeduction);
        deductions.setTardinessDeduction(tardinessDeduction);
        deductions.setUndertimeDeduction(undertimeDeduction);
        employeePayrollDetails.setDeductions(deductions);
        employeePayrollDetails.setAllowances(employeeDetails.getEmploymentDetails().getAllowances());

        employeePayrollDetails.setGrossSalary(computeGrossSalary(employeeDetails));
        employeePayrollDetails.setEmployeeKey(employeeDetails.getEmployeeKey());
        computeTaxDeduction(employeePayrollDetails);

        if (StringUtils.isEmpty(employeeDetails.getDateFromStr())
                || StringUtils.isEmpty(employeeDetails.getDateToStr())) {
            throw new PayrollModuleException(
                    ExceptionMessageConstants.NULL_DATE);
        }

        try {
            employeePayrollDetails.setDateFrom(format.parse(employeeDetails.getDateFromStr()));
            employeePayrollDetails.setDateTo(format.parse(employeeDetails.getDateToStr()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        computeNetSalary(employeePayrollDetails);
        employeePayrollDAO.put(modelMapper.map(employeePayrollDetails,
                EmployeePayrollModel.class));
        return employeePayrollDetails;
    }

    @Override
    public List<EmployeePayrollDetailsDTO> getAllSalary(final String key)
            throws PayrollModuleException {
        List<EmployeePayrollModel> results = employeePayrollDAO
                .getPayrollDetailsByEmployee(key);
        List<EmployeePayrollDetailsDTO> employeePayrollDetailsList = Lists
                .newArrayList();
        if (CollectionUtils.isEmpty(results)) {
            throw new PayrollModuleException(
                    ExceptionMessageConstants.NO_CONTENTS_FOUND);
        }
        for (EmployeePayrollModel model : results) {
            employeePayrollDetailsList.add(modelMapper.map(model,
                    EmployeePayrollDetailsDTO.class));
        }
        return employeePayrollDetailsList;
    }

    private double computeGrossSalary(final EmployeeDetailsDTO employeeDetails) {
        double grossSalary = 0d;
        if (employeeDetails.getHolidayPayRules() != null) {
            int workingDaysCount = 0;
            for (final EmployeeTimeAndAttendanceDTO taa : employeeDetails.getTimeAndAttendanceList()) {
                if (!taa.isHoliday()) {
                    workingDaysCount++;
                }
            }
            grossSalary = employeeDetails.getEmploymentDetails().getDailyRate()
                    * workingDaysCount;
        } else {
            grossSalary = employeeDetails.getEmploymentDetails().getDailyRate()
                    * employeeDetails.getTimeAndAttendanceList().size();
        }
        return grossSalary;
    }

    private void computeNetSalary(
            final EmployeePayrollDetailsDTO employeePayrollDetails) {
        double netSalary = employeePayrollDetails.getGrossSalary()
                - employeePayrollDetails.getDeductions().getTotalDeduction()
                + employeePayrollDetails.getTotalHolidayPay()
                + employeePayrollDetails.getTotalOvertimePay()
                + employeePayrollDetails.getAllowances().getTotalAllowances();
        employeePayrollDetails.setNetSalary(netSalary);
    }

    private double computeHolidayPay(final double dailyMinimumHours,
        final Map<HolidayTypeEnum, HolidayPayRulesDTO> holidayPayRules,
        final EmploymentDetailsDTO employmentDetails,
        final EmployeeTimeAndAttendanceDTO employeAttendance) {
        double holidayPay = 0d;
        if (employeAttendance.isHoliday()) {
            if (employeAttendance.getRenderedHours() > dailyMinimumHours) {
                holidayPay += (dailyMinimumHours * employmentDetails.getHourlyRate())
                        * holidayPayRules.get(
                                employeAttendance.getHolidayType()).getRate();
            } else {
                final double unworkedHours = dailyMinimumHours - employeAttendance.getRenderedHours();
                holidayPay += (employeAttendance.getRenderedHours() * employmentDetails.getHourlyRate())
                        * holidayPayRules.get(
                                employeAttendance.getHolidayType()).getRate();
                holidayPay += computeForUnworkedHoliday(unworkedHours, employmentDetails.getHourlyRate());
            }
        }
        return holidayPay;
    }

    private double computeForUnworkedHoliday(final double renderedHours, final double rate) {
        return renderedHours *  rate;
    }

    // JP
    /*
     * This method is responsible for overtime computation.
     * Implementation: For the prototyping phase, OT rate supported
     * is for REGULAR, REGULAR_HOLIDAY and SPECIAL_HOLIDAY. Assumption
     * is employee is working only from M to F.
     */
    private double computeOvertimePay(final double dailyMinimumHours,
        final Map<OvertimeTypeEnum, OvertimeRulesDTO> overtimeRules,
        final EmploymentDetailsDTO employmentDetails,
        final EmployeeTimeAndAttendanceDTO employeeAttendance) {
        final double overtimeHours = (employeeAttendance.getRenderedHours()
                + (employeeAttendance.getTardiness() / ServiceConstants.SIXTY_MINUTES))
                - dailyMinimumHours;
        double overtimePay = 0d;
        double rate = overtimeRules.get(OvertimeTypeEnum.DAY).getOvertimeRate();
        if (overtimeHours >= 1) {
            if (employeeAttendance.isHoliday()) {
                if (HolidayTypeEnum.REGULAR.equals(employeeAttendance.getHolidayType())) {
                    rate = overtimeRules.get(OvertimeTypeEnum.REGULAR_HOLIDAY).getOvertimeRate();
                } else {
                    rate = overtimeRules.get(OvertimeTypeEnum.SPECIAL_HOLIDAY).getOvertimeRate();
                }
            }
            overtimePay += overtimeHours * (employmentDetails.getHourlyRate() * rate);
            //totalOvertimeHours += overtimeHours;
        }
        return overtimePay;
    }
    // Cicci
    // CSOFF: MagicNumber
    private double computeTardinessDeduction(final double tardiness,
        final EmploymentDetailsDTO employementDetails) {
        double tardinessDeduction = 0d;
        if (tardiness > 0) {
            tardinessDeduction += (tardiness
                * (employementDetails.getHourlyRate() / ServiceConstants.SIXTY_MINUTES));
        }
        return tardinessDeduction;
    }
    // Cicci
    private double computeUndertimeDeduction(final double undertime,
        final EmploymentDetailsDTO employmentDetails) {
        double undertimeDeduction = 0d;
        if (undertime > 0) {
            undertimeDeduction += (undertime
                * (employmentDetails.getHourlyRate() / ServiceConstants.SIXTY_MINUTES));
        }
        return undertimeDeduction;
    }
    // CSON: MagicNumber

    private double computeAbsencesDeduction(final EmploymentDetailsDTO employmentDetails,
        final EmployeeTimeAndAttendanceDTO employeeAttendance) {
        double absencesDeduction = 0d;
        if (!employeeAttendance.isHoliday()
                && employeeAttendance.getRenderedHours() <= 0) {
            absencesDeduction += employmentDetails.getDailyRate();
        }
        return absencesDeduction;
    }
    private void computeTaxDeduction(final EmployeePayrollDetailsDTO payrollDetails) {
        double incomeTaxDeduction = payrollDetails.getGrossSalary() * payrollDetails.getTaxRate();
        payrollDetails.getDeductions().setIncomeTaxDeduction(incomeTaxDeduction);
    }

}
