package com.cloudsherpas.payroll.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;

import com.cloudsherpas.payroll.constants.ResourceConstants;
import com.cloudsherpas.payroll.dto.core.EmployeeDetailsDTO;
import com.cloudsherpas.payroll.dto.core.EmployeePayrollDetailsDTO;
import com.cloudsherpas.payroll.exception.PayrollModuleException;
import com.cloudsherpas.payroll.service.EmployeePayrollService;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.config.Named;

@Api(name = ResourceConstants.API_NAME, version = ResourceConstants.API_VERSION,
    namespace = @ApiNamespace(ownerDomain = ResourceConstants.API_NAMESPACE_OWNER_DOMAIN,
    ownerName = ResourceConstants.API_NAMESPACE_OWNER_NAME),
    description = ResourceConstants.API_DESCRIPTION)
public class PayrollResource {

    @Autowired
    @Qualifier("employeePayrollService")
    @Lazy
    private EmployeePayrollService employeePayrollService;

    /**
     * This api is responsible for computing the salary based on the fields
     * provided in the EmployeeDetailsDTO
     *
     * @param employeeDTO
     *            All fields of the DTO is required for computation.
     * @return response
     * @throws PayrollModuleException
     */
    @ApiMethod(name = "employees.salaries.create", path = "payroll/employee/salary", httpMethod = ApiMethod.HttpMethod.POST)
    public EmployeePayrollDetailsDTO computeEmployeeSalary(
            final EmployeeDetailsDTO employeeDTO) throws PayrollModuleException {
        return employeePayrollService.computeSalaryforEmployee(employeeDTO);

    }

    @ApiMethod(name = "employees.salaries.list", path = "payroll/employee/payslips/list", httpMethod = ApiMethod.HttpMethod.GET)
    public List<EmployeePayrollDetailsDTO> listEmployeeSalary(
            @Named("id") final String key) throws PayrollModuleException {
        return employeePayrollService.getAllSalary(key);
    }
}

