package com.cloudsherpas.payroll.dao;

import com.cloudsherpas.payroll.models.EmployeePayrollModel;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.impl.translate.opt.joda.JodaTimeTranslators;

public final class DAOManager {

    private static DAOManager self;

    static {
        registerEntities();
    }

    private DAOManager() {
    }

    public static DAOManager getInstance() {
        if (self == null) {
            self = new DAOManager();
        }

        return self;
    }

    private static void registerEntities() {
        ObjectifyService.begin();

        JodaTimeTranslators.add(ObjectifyService.factory());

        ObjectifyService.factory().register(EmployeePayrollModel.class);
    }

    public Objectify getObjectify() {
        return ObjectifyService.ofy();
    }
}
