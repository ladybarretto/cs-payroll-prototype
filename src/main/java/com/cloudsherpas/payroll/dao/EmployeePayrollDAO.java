package com.cloudsherpas.payroll.dao;

import java.util.List;

import com.cloudsherpas.payroll.models.EmployeePayrollModel;

public interface EmployeePayrollDAO extends BaseDAO<EmployeePayrollModel> {

    List<EmployeePayrollModel> getPayrollDetailsByEmployee(final String employeeKey);
}
