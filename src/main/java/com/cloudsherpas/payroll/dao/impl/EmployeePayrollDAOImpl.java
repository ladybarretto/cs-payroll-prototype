package com.cloudsherpas.payroll.dao.impl;

import java.util.List;

import com.cloudsherpas.payroll.dao.DAOManager;
import com.cloudsherpas.payroll.dao.EmployeePayrollDAO;
import com.cloudsherpas.payroll.models.EmployeePayrollModel;
import com.googlecode.objectify.Objectify;

public class EmployeePayrollDAOImpl extends BaseDAOImpl<EmployeePayrollModel> implements EmployeePayrollDAO {
    private static final DAOManager DAO_MANAGER = DAOManager.getInstance();

    public EmployeePayrollDAOImpl() {
        super(EmployeePayrollModel.class);
    }

    @Override
    public     List<EmployeePayrollModel> getPayrollDetailsByEmployee(final String employeeKey) {
        final Objectify ofy = DAO_MANAGER.getObjectify();
        return ofy.load().type(EmployeePayrollModel.class).filter("employeeKey", employeeKey).list();

    }
}
