package com.cloudsherpas.payroll.enums;

public enum OvertimeTypeEnum {
    DAY,
    NIGHT,
    RESTDAY,
    REGULAR_HOLIDAY,
    SPECIAL_HOLIDAY,
    RESTDAY_REGULAR_HOLIDAY,
    RESTDAY_SPECIAL_HOLIDAY,
    ACCUMULATED;

}
