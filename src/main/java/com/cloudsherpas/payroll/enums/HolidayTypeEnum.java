package com.cloudsherpas.payroll.enums;

public enum HolidayTypeEnum {
    SPECIAL_NON_WORKING,
    REGULAR
}
