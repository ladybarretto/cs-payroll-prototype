package com.cloudsherpas.payroll.enums;

public enum PayRulesMultiplierEnum {
    HOURLY, DAILY, MONTHLY;
}
