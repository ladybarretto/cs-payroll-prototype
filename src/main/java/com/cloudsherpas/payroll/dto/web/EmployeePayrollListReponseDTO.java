package com.cloudsherpas.payroll.dto.web;

import java.util.List;

import com.cloudsherpas.payroll.dto.core.EmployeePayrollDetailsDTO;

public class EmployeePayrollListReponseDTO extends BaseResponseDTO {

    private int resultCount;
    private List<EmployeePayrollDetailsDTO> data;
    public int getResultCount() {
        return resultCount;
    }
    public void setResultCount(final int resultCount) {
        this.resultCount = resultCount;
    }
    public List<EmployeePayrollDetailsDTO> getData() {
        return data;
    }
    public void setData(final List<EmployeePayrollDetailsDTO> data) {
        this.data = data;
    }
}
