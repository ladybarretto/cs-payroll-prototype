package com.cloudsherpas.payroll.dto.web;

public class BaseResponseDTO {
    private String statusMessage;
    private int statusCode;
    public String getStatusMessage() {
        return statusMessage;
    }
    public void setStatusMessage(final String statusMessage) {
        this.statusMessage = statusMessage;
    }
    public int getStatusCode() {
        return statusCode;
    }
    public void setStatusCode(final int statusCode) {
        this.statusCode = statusCode;
    }

}
