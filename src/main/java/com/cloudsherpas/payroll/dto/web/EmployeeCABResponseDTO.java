package com.cloudsherpas.payroll.dto.web;

import com.cloudsherpas.payroll.dto.core.EmployeeCompensationAndBenefit;

public class EmployeeCABResponseDTO extends BaseResponseDTO {

    private EmployeeCompensationAndBenefit data;

    public EmployeeCompensationAndBenefit getData() {
        return data;
    }

    public void setData(final EmployeeCompensationAndBenefit data) {
        this.data = data;
    }
}
