package com.cloudsherpas.payroll.dto.web;

import com.cloudsherpas.payroll.dto.core.EmployeePayrollDetailsDTO;

public class EmployeePayrollResponseDTO extends BaseResponseDTO {

    private EmployeePayrollDetailsDTO data;

    public EmployeePayrollDetailsDTO getData() {
        return data;
    }

    public void setData(final EmployeePayrollDetailsDTO data) {
        this.data = data;
    }
}
