package com.cloudsherpas.payroll.dto.core;

public class HolidayPayRulesDTO {

    private double rate;

    public double getRate() {
        return rate;
    }

    public void setRate(final double rate) {
        this.rate = rate;
    }

}
