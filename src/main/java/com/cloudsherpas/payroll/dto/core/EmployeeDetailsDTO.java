package com.cloudsherpas.payroll.dto.core;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.cloudsherpas.payroll.enums.HolidayTypeEnum;
import com.cloudsherpas.payroll.enums.OvertimeTypeEnum;

public class EmployeeDetailsDTO {

    private String employeeKey;
    private String countryCode;
    private String civilStatus;
    private int numberOfDependents;
    private Date dateTo;
    private Date dateFrom;
    private String dateToStr;
    private String dateFromStr;

    //new
    private Map<OvertimeTypeEnum, OvertimeRulesDTO> overtimeRules;
    private WorkingTimeRulesDTO workingTimeRules;
    private Map<HolidayTypeEnum, HolidayPayRulesDTO> holidayPayRules;
    private List<EmployeeTimeAndAttendanceDTO> timeAndAttendanceList;
    private EmploymentDetailsDTO employmentDetails;

    public void setEmployeeKey(final String employeeKey) {
        this.employeeKey = employeeKey;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(final String countryCode) {
        this.countryCode = countryCode;
    }

    public String getEmployeeKey() {
        return employeeKey;
    }

    public String getCivilStatus() {
        return civilStatus;
    }

    public void setCivilStatus(final String civilStatus) {
        this.civilStatus = civilStatus;
    }

    public int getNumberOfDependents() {
        return numberOfDependents;
    }

    public void setNumberOfDependents(final int numberOfDependents) {
        this.numberOfDependents = numberOfDependents;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(final Date dateTo) {
        this.dateTo = dateTo;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(final Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getDateToStr() {
        return dateToStr;
    }

    public void setDateToStr(final String dateToStr) {
        this.dateToStr = dateToStr;
    }

    public String getDateFromStr() {
        return dateFromStr;
    }

    public void setDateFromStr(final String dateFromStr) {
        this.dateFromStr = dateFromStr;
    }

    public Map<OvertimeTypeEnum, OvertimeRulesDTO> getOvertimeRules() {
        return overtimeRules;
    }

    public void setOvertimeRules(
            final Map<OvertimeTypeEnum, OvertimeRulesDTO> overtimeRules) {
        this.overtimeRules = overtimeRules;
    }

    public WorkingTimeRulesDTO getWorkingTimeRules() {
        return workingTimeRules;
    }

    public void setWorkingTimeRules(final WorkingTimeRulesDTO workingTimeRules) {
        this.workingTimeRules = workingTimeRules;
    }

    public Map<HolidayTypeEnum, HolidayPayRulesDTO> getHolidayPayRules() {
        return holidayPayRules;
    }

    public void setHolidayPayRules(
            final Map<HolidayTypeEnum, HolidayPayRulesDTO> holidayPayRules) {
        this.holidayPayRules = holidayPayRules;
    }

    public List<EmployeeTimeAndAttendanceDTO> getTimeAndAttendanceList() {
        return timeAndAttendanceList;
    }

    public void setTimeAndAttendanceList(
            final List<EmployeeTimeAndAttendanceDTO> timeAndAttendanceList) {
        this.timeAndAttendanceList = timeAndAttendanceList;
    }

    public EmploymentDetailsDTO getEmploymentDetails() {
        return employmentDetails;
    }

    public void setEmploymentDetails(final EmploymentDetailsDTO employmentDetails) {
        this.employmentDetails = employmentDetails;
    }
}
