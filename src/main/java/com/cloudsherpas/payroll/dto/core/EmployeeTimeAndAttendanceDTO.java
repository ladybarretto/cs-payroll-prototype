package com.cloudsherpas.payroll.dto.core;

import java.util.Date;

import com.cloudsherpas.payroll.enums.HolidayTypeEnum;

public class EmployeeTimeAndAttendanceDTO {

    private Date workingDate;
    private boolean isHoliday;
    private HolidayTypeEnum holidayType;
    private double renderedHours;
    private double tardiness;
    private double undertime;

    public boolean isHoliday() {
        return isHoliday;
    }
    public void setHoliday(final boolean isHolidayFlag) {
        this.isHoliday = isHolidayFlag;
    }
    public HolidayTypeEnum getHolidayType() {
        return holidayType;
    }
    public void setHolidayType(final HolidayTypeEnum holidayType) {
        this.holidayType = holidayType;
    }
    public double getRenderedHours() {
        return renderedHours;
    }
    public void setRenderedHours(final double renderedHours) {
        this.renderedHours = renderedHours;
    }
    public double getTardiness() {
        return tardiness;
    }
    public void setTardiness(final double tardiness) {
        this.tardiness = tardiness;
    }
    public double getUndertime() {
        return undertime;
    }
    public void setUndertime(final double undertime) {
        this.undertime = undertime;
    }
    public Date getWorkingDate() {
        return workingDate;
    }
    public void setWorkingDate(final Date workingDate) {
        this.workingDate = workingDate;
    }
}
