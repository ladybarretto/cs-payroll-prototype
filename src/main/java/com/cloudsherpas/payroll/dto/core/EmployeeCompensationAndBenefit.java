package com.cloudsherpas.payroll.dto.core;

public class EmployeeCompensationAndBenefit {

    private double dailyRate;
    private double monthlyRate;
    private double hourlyRate;

    private double gasAllowance;
    private double communicationAllowance;
    private double riceAllowance;
    private double clothingAllowance;

    public double getDailyRate() {
        return dailyRate;
    }
    public void setDailyRate(final double dailyRate) {
        this.dailyRate = dailyRate;
    }
    public double getMonthlyRate() {
        return monthlyRate;
    }
    public void setMonthlyRate(final double monthlyRate) {
        this.monthlyRate = monthlyRate;
    }
    public double getHourlyRate() {
        return hourlyRate;
    }
    public void setHourlyRate(final double hourlyRate) {
        this.hourlyRate = hourlyRate;
    }
    public double getGasAllowance() {
        return gasAllowance;
    }
    public void setGasAllowance(final double gasAllowance) {
        this.gasAllowance = gasAllowance;
    }
    public double getCommunicationAllowance() {
        return communicationAllowance;
    }
    public void setCommunicationAllowance(final double communicationAllowance) {
        this.communicationAllowance = communicationAllowance;
    }
    public double getRiceAllowance() {
        return riceAllowance;
    }
    public void setRiceAllowance(final double riceAllowance) {
        this.riceAllowance = riceAllowance;
    }
    public double getClothingAllowance() {
        return clothingAllowance;
    }
    public void setClothingAllowance(final double clothingAllowance) {
        this.clothingAllowance = clothingAllowance;
    }

}
