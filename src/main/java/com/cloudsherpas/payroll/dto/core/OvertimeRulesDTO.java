package com.cloudsherpas.payroll.dto.core;

import com.cloudsherpas.payroll.enums.PayRulesMultiplierEnum;

public class OvertimeRulesDTO {

    private double overtimeRate;
    private PayRulesMultiplierEnum multiplier;
    private double minNumberOfOvertimeHours;
    private double maxNumberOfOvertimeHours;

    public double getOvertimeRate() {
        return overtimeRate;
    }
    public void setOvertimeRate(final double overtimeRate) {
        this.overtimeRate = overtimeRate;
    }
    public PayRulesMultiplierEnum getMultiplier() {
        return multiplier;
    }
    public void setMultiplier(final PayRulesMultiplierEnum multiplier) {
        this.multiplier = multiplier;
    }
    public double getMinNumberOfOvertimeHours() {
        return minNumberOfOvertimeHours;
    }
    public void setMinNumberOfOvertimeHours(final double minNumberOfOvertimeHours) {
        this.minNumberOfOvertimeHours = minNumberOfOvertimeHours;
    }
    public double getMaxNumberOfOvertimeHours() {
        return maxNumberOfOvertimeHours;
    }
    public void setMaxNumberOfOvertimeHours(final double maxNumberOfOvertimeHours) {
        this.maxNumberOfOvertimeHours = maxNumberOfOvertimeHours;
    }

    @Override
    public String toString() {
        return "OvertimeRulesDTO [overtimeRate=" + overtimeRate
                + ", multiplier=" + multiplier + ", minNumberOfOvertimeHours="
                + minNumberOfOvertimeHours + ", maxNumberOfOvertimeHours="
                + maxNumberOfOvertimeHours + "]";
    }

}
