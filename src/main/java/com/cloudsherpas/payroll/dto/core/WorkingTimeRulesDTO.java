package com.cloudsherpas.payroll.dto.core;

public class WorkingTimeRulesDTO {

    private double dailyMinimumWorkHours;
    private double weeklyMinimumWorkHours;
    public double getDailyMinimumWorkHours() {
        return dailyMinimumWorkHours;
    }
    public void setDailyMinimumWorkHours(final double dailyMinimumWorkHours) {
        this.dailyMinimumWorkHours = dailyMinimumWorkHours;
    }
    public double getWeeklyMinimumWorkHours() {
        return weeklyMinimumWorkHours;
    }
    public void setWeeklyMinimumWorkHours(final double weeklyMinimumWorkHours) {
        this.weeklyMinimumWorkHours = weeklyMinimumWorkHours;
    }

}
