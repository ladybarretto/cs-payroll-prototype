package com.cloudsherpas.payroll.dto.core;

public class EmploymentDetailsDTO {
    private int jobGradeLevel;
    private String jobPositionCode;
    private double dailyRate;
    private double monthlyRate;
    private double hourlyRate;
    private String jobPositionName;
    private AllowancesDTO allowances;

    public int getJobGradeLevel() {
        return jobGradeLevel;
    }
    public void setJobGradeLevel(final int jobGradeLevel) {
        this.jobGradeLevel = jobGradeLevel;
    }
    public String getJobPositionCode() {
        return jobPositionCode;
    }
    public void setJobPositionCode(final String jobPositionCode) {
        this.jobPositionCode = jobPositionCode;
    }
    public double getDailyRate() {
        return dailyRate;
    }
    public void setDailyRate(final double dailyRate) {
        this.dailyRate = dailyRate;
    }
    public double getMonthlyRate() {
        return monthlyRate;
    }
    public void setMonthlyRate(final double monthlyRate) {
        this.monthlyRate = monthlyRate;
    }
    public double getHourlyRate() {
        return hourlyRate;
    }
    public void setHourlyRate(final double hourlyRate) {
        this.hourlyRate = hourlyRate;
    }
    public String getJobPositionName() {
        return jobPositionName;
    }
    public void setJobPositionName(final String jobPositionName) {
        this.jobPositionName = jobPositionName;
    }
    public AllowancesDTO getAllowances() {
        return allowances;
    }
    public void setAllowances(final AllowancesDTO allowances) {
        this.allowances = allowances;
    }


}
