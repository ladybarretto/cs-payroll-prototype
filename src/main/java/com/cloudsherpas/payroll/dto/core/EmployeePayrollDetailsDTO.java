package com.cloudsherpas.payroll.dto.core;

import java.util.Date;

public class EmployeePayrollDetailsDTO {

    private String employeeKey;
    private double dailyRate;
    private double monthlyRate;
    private double hourlyRate;
    private double grossSalary;
    private double netSalary;
    private double taxRate;
    private double totalOvertimePay;
    private double totalOvertimeHours;
    private double totalHolidayPay;

    private Date dateTo;
    private Date dateFrom;
    private String dateToStr;
    private String dateFromStr;

    private EmployeePayrollDeductionsDTO deductions;
    private AllowancesDTO allowances;

    public String getEmployeeKey() {
        return employeeKey;
    }
    public void setEmployeeKey(final String employeeKey) {
        this.employeeKey = employeeKey;
    }
    public double getDailyRate() {
        return dailyRate;
    }
    public void setDailyRate(final double dailyRate) {
        this.dailyRate = dailyRate;
    }
    public EmployeePayrollDeductionsDTO getDeductions() {
        return deductions;
    }
    public void setDeductions(final EmployeePayrollDeductionsDTO deductions) {
        this.deductions = deductions;
    }
    public double getGrossSalary() {
        return grossSalary;
    }
    public void setGrossSalary(final double grossSalary) {
        this.grossSalary = grossSalary;
    }
    public double getNetSalary() {
        return netSalary;
    }
    public void setNetSalary(final double netSalary) {
        this.netSalary = netSalary;
    }
    public double getTaxRate() {
        return taxRate;
    }
    public void setTaxRate(final double taxRate) {
        this.taxRate = taxRate;
    }

    public double getMonthlyRate() {
        return monthlyRate;
    }
    public void setMonthlyRate(final double monthlyRate) {
        this.monthlyRate = monthlyRate;
    }
    public double getHourlyRate() {
        return hourlyRate;
    }
    public void setHourlyRate(final double hourlyRate) {
        this.hourlyRate = hourlyRate;
    }
    public Date getDateTo() {
        return dateTo;
    }
    public void setDateTo(final Date dateTo) {
        this.dateTo = dateTo;
    }
    public Date getDateFrom() {
        return dateFrom;
    }
    public void setDateFrom(final Date dateFrom) {
        this.dateFrom = dateFrom;
    }
    public String getDateFromStr() {
        return dateFromStr;
    }
    public void setDateFromStr(final String dateFromStr) {
        this.dateFromStr = dateFromStr;
    }
    public String getDateToStr() {
        return dateToStr;
    }
    public void setDateToStr(final String dateToStr) {
        this.dateToStr = dateToStr;
    }
    public double getTotalOvertimePay() {
        return totalOvertimePay;
    }
    public void setTotalOvertimePay(final double totalOvertimePay) {
        this.totalOvertimePay = totalOvertimePay;
    }
    public double getTotalOvertimeHours() {
        return totalOvertimeHours;
    }
    public void setTotalOvertimeHours(final double totalOvertimeHours) {
        this.totalOvertimeHours = totalOvertimeHours;
    }
    public double getTotalHolidayPay() {
        return totalHolidayPay;
    }
    public void setTotalHolidayPay(final double totalHolidayPay) {
        this.totalHolidayPay = totalHolidayPay;
    }
    public AllowancesDTO getAllowances() {
        return allowances;
    }
    public void setAllowances(final AllowancesDTO allowances) {
        this.allowances = allowances;
    }
}
