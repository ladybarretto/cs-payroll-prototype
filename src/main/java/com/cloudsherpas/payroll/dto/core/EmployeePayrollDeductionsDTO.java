package com.cloudsherpas.payroll.dto.core;


public class EmployeePayrollDeductionsDTO {

    private double incomeTaxDeduction;
    private double sssDeduction;
    private double philHealthDeduction;
    private double pagibigDeduction;
    private double absencesDeduction;
    private double tardinessDeduction;
    private double undertimeDeduction;

    public double getIncomeTaxDeduction() {
        return incomeTaxDeduction;
    }
    public void setIncomeTaxDeduction(final double incomeTaxDeduction) {
        this.incomeTaxDeduction = incomeTaxDeduction;
    }
    public double getSssDeduction() {
        return sssDeduction;
    }
    public void setSssDeduction(final double sssDeduction) {
        this.sssDeduction = sssDeduction;
    }
    public double getPhilHealthDeduction() {
        return philHealthDeduction;
    }
    public void setPhilHealthDeduction(final double philHealthDeduction) {
        this.philHealthDeduction = philHealthDeduction;
    }
    public double getPagibigDeduction() {
        return pagibigDeduction;
    }
    public void setPagibigDeduction(final double pagibigDeduction) {
        this.pagibigDeduction = pagibigDeduction;
    }
    public double getAbsencesDeduction() {
        return absencesDeduction;
    }
    public void setAbsencesDeduction(final double absencesDeduction) {
        this.absencesDeduction = absencesDeduction;
    }

    public double getTotalDeduction() {
        double totalDeductions = this.absencesDeduction + this.incomeTaxDeduction
            + this.pagibigDeduction + this.philHealthDeduction
            + this.sssDeduction + this.tardinessDeduction + this.undertimeDeduction;
        return totalDeductions;
    }
    public double getTardinessDeduction() {
        return tardinessDeduction;
    }
    public void setTardinessDeduction(final double tardinessDeduction) {
        this.tardinessDeduction = tardinessDeduction;
    }
    public double getUndertimeDeduction() {
        return undertimeDeduction;
    }
    public void setUndertimeDeduction(final double undertimeDeduction) {
        this.undertimeDeduction = undertimeDeduction;
    }

}
