package com.cloudsherpas.payroll.dto.core;

public class AllowancesDTO {
    private double travelAllowance;
    private double communicationAllowance;
    private double gasAllowance;
    private double clothingAllowance;
    public double getTravelAllowance() {
        return travelAllowance;
    }
    public void setTravelAllowance(final double travelAllowance) {
        this.travelAllowance = travelAllowance;
    }
    public double getCommunicationAllowance() {
        return communicationAllowance;
    }
    public void setCommunicationAllowance(final double communicationAllowance) {
        this.communicationAllowance = communicationAllowance;
    }
    public double getGasAllowance() {
        return gasAllowance;
    }
    public void setGasAllowance(final double gasAllowance) {
        this.gasAllowance = gasAllowance;
    }
    public double getClothingAllowance() {
        return clothingAllowance;
    }
    public void setClothingAllowance(final double clothingAllowance) {
        this.clothingAllowance = clothingAllowance;
    }
    public double getTotalAllowances() {
        double totalAllowances = this.travelAllowance + this.communicationAllowance
            + this.gasAllowance + this.clothingAllowance;
        return totalAllowances;
    }

}
