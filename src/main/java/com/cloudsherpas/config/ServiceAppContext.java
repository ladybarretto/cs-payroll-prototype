package com.cloudsherpas.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import com.cloudsherpas.payroll.service.EmployeePayrollRulesService;
import com.cloudsherpas.payroll.service.EmployeePayrollService;
import com.cloudsherpas.payroll.service.impl.EmployeePayrollRulesServiceImpl;
import com.cloudsherpas.payroll.service.impl.EmployeePayrollServiceImpl;

@Configuration
@Lazy
public class ServiceAppContext {

    @Bean(name = "employeePayrollService")
    public EmployeePayrollService getEmployeePayrollService() {
        return new EmployeePayrollServiceImpl();
    }

    @Bean(name = "employeePayrollRulesService")
    public EmployeePayrollRulesService getEmployeePayrollRulesService() {
        return new EmployeePayrollRulesServiceImpl();
    }

}
