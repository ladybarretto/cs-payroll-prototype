package com.cloudsherpas.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import com.cloudsherpas.payroll.dao.EmployeePayrollDAO;
import com.cloudsherpas.payroll.dao.impl.EmployeePayrollDAOImpl;

@Configuration
@Lazy
public class DAOAppContext {

    @Bean(name = "employeePayrollDAO")
    public EmployeePayrollDAO getEmployeePayrollDAO() {
        return new EmployeePayrollDAOImpl();
    }
}
