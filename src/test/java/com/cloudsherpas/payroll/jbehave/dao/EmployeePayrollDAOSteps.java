package com.cloudsherpas.payroll.jbehave.dao;

import java.util.ArrayList;
import java.util.List;

import org.jbehave.core.annotations.*;
import org.junit.Assert;

import com.cloudsherpas.payroll.dao.EmployeePayrollDAO;
import com.cloudsherpas.payroll.dao.impl.EmployeePayrollDAOImpl;
import com.cloudsherpas.payroll.models.EmployeePayrollModel;
import com.cloudsherpas.payroll.models.EmployeeSalaryDeductionModel;
import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

public class EmployeePayrollDAOSteps {

    EmployeePayrollDAO dao;
    EmployeePayrollModel model;
    List<EmployeePayrollModel> expectedList = new ArrayList<EmployeePayrollModel>();

    private final LocalServiceTestHelper helper = new LocalServiceTestHelper(
            new LocalDatastoreServiceTestConfig());

    @BeforeScenario
    public void beforeScenario() {
        model = new EmployeePayrollModel();
        model.setDailyRate(2400d);
        EmployeeSalaryDeductionModel deductions = new EmployeeSalaryDeductionModel();
        deductions.setAbsencesDeduction(2000d);
        deductions.setIncomeTaxDeduction(1500d);
        deductions.setPagibigDeduction(100d);
        deductions.setPhilHealthDeduction(500d);
        deductions.setSssDeduction(300d);
        model.setDeductions(deductions);
        model.setEmployeeKey("empKey1");
        model.setGrossSalary(30000d);
        model.setNetSalary(20000d);
        model.setOvertimePay(0d);
        model.setTaxRate(32d);
        model.setId(1111L);
        dao = new EmployeePayrollDAOImpl();
        helper.setUp();
    }

    @Given("Employee Payroll details is saved in the database with the employee key of empKey1")
    public void givenEmployeePayrollDetailsIsSavedInTheDatabaseWithTheEmployeeKeyOf1234() {
        Long result = dao.put(model);
        Assert.assertEquals(1111, result.longValue());
    }

    @When("I retrieving employee payroll details based on employee key empKey1")
    public void whenIRetrievingEmployeePayrollDetailsBasedOnEmployeeKey1234() {
        expectedList = dao.getPayrollDetailsByEmployee("empKey1");
    }

    @Then("Employee payroll details is being returned")
    public void thenEmployeePayrollDetailsIsBeingReturned() {
        Assert.assertEquals(1, expectedList.size());
    }
}