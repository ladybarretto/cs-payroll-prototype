package com.cloudsherpas.payroll.jbehave;

import java.util.List;

import net.serenitybdd.jbehave.SerenityStory;

import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.io.CodeLocations;
import org.jbehave.core.io.LoadFromClasspath;
import org.jbehave.core.io.StoryFinder;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;

import com.cloudsherpas.payroll.api.PayrollResource;
import com.cloudsherpas.payroll.jbehave.api.PayrollResourceStepsTest;
import com.cloudsherpas.payroll.jbehave.dao.EmployeePayrollDAOSteps;
import com.cloudsherpas.payroll.jbehave.service.EmployeePayrollRulesServiceStepsTest;

public class BaseBDDStepsTest extends SerenityStory {
    @Override
    public Configuration configuration() {
        return new MostUsefulConfiguration().useStoryLoader(
                new LoadFromClasspath(this.getClass()))
                .useStoryReporterBuilder(
                        new StoryReporterBuilder().withDefaultFormats()
                                .withFormats(
                                        StoryReporterBuilder.Format.CONSOLE,
                                        StoryReporterBuilder.Format.TXT));
    }

    @Override
    public List<String> storyPaths() {
        return new StoryFinder().findPaths(
                CodeLocations.codeLocationFromClass(this.getClass()),
                "**/*.story", "");
    }
    
	@Override
	public InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(),
                new EmployeePayrollRulesServiceStepsTest(),
                new EmployeePayrollDAOSteps());
	}
}
