package com.cloudsherpas.payroll.jbehave;


import java.util.List;

import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.embedder.Embedder;
import org.jbehave.core.io.CodeLocations;
import org.jbehave.core.io.StoryFinder;
import org.jbehave.core.io.StoryPathResolver;
import org.jbehave.core.junit.JUnitStory;
import org.jbehave.core.steps.CandidateSteps;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.jbehave.core.steps.spring.SpringStepsFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Lazy;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.cloudsherpas.config.ServiceAppContext;
import com.cloudsherpas.payroll.api.PayrollResource;
import com.cloudsherpas.payroll.dao.EmployeePayrollDAO;
import com.cloudsherpas.payroll.jbehave.dao.EmployeePayrollDAOSteps;
import com.cloudsherpas.payroll.jbehave.service.EmployeePayrollRulesServiceStepsTest;
import com.cloudsherpas.payroll.jbehave.service.EmployeePayrollServiceStepsTest;
import com.cloudsherpas.payroll.service.EmployeePayrollRulesService;
import com.cloudsherpas.payroll.service.EmployeePayrollService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=ServiceAppContext.class)
public class BaseSpringTest extends JUnitStory {

    @Autowired
    @Qualifier("employeePayrollRulesService")
    @Lazy
    private EmployeePayrollRulesService employeePayrollRulesService;
    
    @Autowired
    @Qualifier("employeePayrollService")
    @Lazy
    private EmployeePayrollService employeePayrollService;

    @Autowired private ApplicationContext context;
    
    @Override
    public Configuration configuration() {
        return new MostUsefulConfiguration();
    }

    
    @Override public List<CandidateSteps> candidateSteps() {
        return new SpringStepsFactory(configuration(), context).createCandidateSteps();
    }
}