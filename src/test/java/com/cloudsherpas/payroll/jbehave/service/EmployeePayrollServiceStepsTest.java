package com.cloudsherpas.payroll.jbehave.service;

import org.jbehave.core.annotations.BeforeScenario;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.cloudsherpas.payroll.dao.EmployeePayrollDAO;
import com.cloudsherpas.payroll.dto.core.AllowancesDTO;
import com.cloudsherpas.payroll.dto.core.EmployeeDetailsDTO;
import com.cloudsherpas.payroll.dto.core.EmployeePayrollDeductionsDTO;
import com.cloudsherpas.payroll.dto.core.EmployeePayrollDetailsDTO;
import com.cloudsherpas.payroll.dto.core.EmploymentDetailsDTO;
import com.cloudsherpas.payroll.exception.PayrollModuleException;
import com.cloudsherpas.payroll.service.EmployeePayrollRulesService;
import com.cloudsherpas.payroll.service.EmployeePayrollService;
import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

@Component
public class EmployeePayrollServiceStepsTest {
    EmployeeDetailsDTO empDetails;
    EmployeePayrollDeductionsDTO employeeDeductions;
    EmployeePayrollDetailsDTO empPayrollDetails;
    EmploymentDetailsDTO employmentDetails;
    AllowancesDTO allowancesDTO;
    @Autowired
    @Qualifier("employeePayrollService")
    @Lazy
    private EmployeePayrollService employeePayrollService;

    private final LocalServiceTestHelper helper = new LocalServiceTestHelper(
            new LocalDatastoreServiceTestConfig());

    @BeforeScenario
	public void beforeScenario() {
        empDetails = new EmployeeDetailsDTO();
        empDetails.setCivilStatus("S");
        empDetails.setCountryCode("PH");
        empDetails.setEmployeeKey("empkey01");
        empDetails.setNumberOfDependents(0);
        
        employmentDetails = new EmploymentDetailsDTO();
        employmentDetails.setDailyRate(3181.82);
        employmentDetails.setMonthlyRate(70000);
        employmentDetails.setHourlyRate(397.73);
        employmentDetails.setJobGradeLevel(1);
        employmentDetails.setJobPositionCode("MGR");
        
        allowancesDTO = new AllowancesDTO();
        allowancesDTO.setClothingAllowance(1500);
        allowancesDTO.setCommunicationAllowance(1799);
        allowancesDTO.setGasAllowance(1000);
        allowancesDTO.setTravelAllowance(1000);
        employmentDetails.setAllowances(allowancesDTO);
        empDetails.setEmploymentDetails(employmentDetails);
        helper.setUp();
	}

    @When("I compute employee salary")
    public void iComputeEmployeeSalary() throws PayrollModuleException {
    	empPayrollDetails = employeePayrollService.computeSalaryforEmployee(empDetails);
    }

    @Then("Calculated employee payroll details is being returned")
    public void calculatedEmployeePayrollDetailsIsBeingReturned() {
        Assert.assertNotNull(empPayrollDetails);
        Assert.assertTrue(1000 == employmentDetails.getAllowances().getTravelAllowance());
    }
	
}
