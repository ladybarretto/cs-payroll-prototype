package com.cloudsherpas.payroll.jbehave.service;
import static org.junit.Assert.assertTrue;

import org.jbehave.core.annotations.*;

import com.cloudsherpas.payroll.dto.core.AllowancesDTO;
import com.cloudsherpas.payroll.dto.core.EmployeeDetailsDTO;
import com.cloudsherpas.payroll.dto.core.EmployeePayrollDeductionsDTO;
import com.cloudsherpas.payroll.dto.core.EmployeePayrollDetailsDTO;
import com.cloudsherpas.payroll.dto.core.EmploymentDetailsDTO;
import com.cloudsherpas.payroll.exception.PayrollModuleException;
import com.cloudsherpas.payroll.service.EmployeePayrollRulesService;
import com.cloudsherpas.payroll.service.impl.EmployeePayrollRulesServiceImpl;

public class EmployeePayrollRulesServiceStepsTest{
	
    EmployeeDetailsDTO empDetails;
    EmployeePayrollDeductionsDTO employeeDeductions;
    EmployeePayrollDetailsDTO empPayrollDetails;
    EmploymentDetailsDTO employmentDetails;
    AllowancesDTO allowancesDTO;
    
    EmployeePayrollRulesService employeePayrollService;
    
	@BeforeScenario
	public void beforeScenario() {
        empDetails = new EmployeeDetailsDTO();
        empDetails.setCivilStatus("S");
        empDetails.setCountryCode("PH");
        empDetails.setEmployeeKey("empkey01");
        empDetails.setNumberOfDependents(0);
        
        employmentDetails = new EmploymentDetailsDTO();
        employmentDetails.setDailyRate(3181.82);
        employmentDetails.setMonthlyRate(70000);
        employmentDetails.setHourlyRate(397.73);
        employmentDetails.setJobGradeLevel(1);
        employmentDetails.setJobPositionCode("MGR");
        
        allowancesDTO = new AllowancesDTO();
        allowancesDTO.setClothingAllowance(1500);
        allowancesDTO.setCommunicationAllowance(1799);
        allowancesDTO.setGasAllowance(1000);
        allowancesDTO.setTravelAllowance(1000);
        employmentDetails.setAllowances(allowancesDTO);
        empDetails.setEmploymentDetails(employmentDetails);
        employeePayrollService = new EmployeePayrollRulesServiceImpl();
	}
	
	@When(" Getting employee salary deductions and tax rate based on decision table rules")
	public void whenGettingEmployeeSalaryDeductionsAndTaxRateBasedOnDecisionTableRules() throws PayrollModuleException{
		empPayrollDetails = employeePayrollService.getEmployeePayrollDetails(empDetails);
	}
	
	@Then("Government deductions and tax rate is returned")
	public void thenGovernmentDeductionsAndTaxRateIsReturned(){
        assertTrue(empPayrollDetails.getDeductions().getSssDeduction()==1024.22);
        assertTrue(empPayrollDetails.getDeductions().getPagibigDeduction()==100);
        assertTrue(empPayrollDetails.getDeductions().getPhilHealthDeduction()==1024.00);
        assertTrue(empPayrollDetails.getTaxRate()==0.32);
	}
}