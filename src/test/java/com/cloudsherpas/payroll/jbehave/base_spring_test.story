Meta:

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario:  Computing and Retrieving employee payroll details
When I compute employee salary
Then Calculated employee payroll details is being returned

Scenario:  Access Compute salary Api method
When I compute employee salary via api resource
Then API response is employee details