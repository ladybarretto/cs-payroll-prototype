package com.cloudsherpas.payroll.jbehave.api;

import org.jbehave.core.annotations.BeforeScenario;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.cloudsherpas.payroll.api.PayrollResource;
import com.cloudsherpas.payroll.dto.core.AllowancesDTO;
import com.cloudsherpas.payroll.dto.core.EmployeeDetailsDTO;
import com.cloudsherpas.payroll.dto.core.EmployeePayrollDetailsDTO;
import com.cloudsherpas.payroll.dto.core.EmploymentDetailsDTO;
import com.cloudsherpas.payroll.exception.PayrollModuleException;
import com.cloudsherpas.payroll.service.EmployeePayrollService;
import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

@Component
public class PayrollResourceStepsTest {

    @Autowired
    @Qualifier("employeePayrollService")
    @Lazy
    private EmployeePayrollService employeePayrollService;
    
	PayrollResource resource = new PayrollResource() ;
	EmployeeDetailsDTO empDetails;
	EmployeePayrollDetailsDTO empResponsePayrollDetails;
	AllowancesDTO allowances;
    EmploymentDetailsDTO employmentDetails;
    private final LocalServiceTestHelper helper = new LocalServiceTestHelper(
            new LocalDatastoreServiceTestConfig());
	@BeforeScenario
	public void beforeScenario() {
        empDetails = new EmployeeDetailsDTO();
        empDetails.setCivilStatus("S");
        empDetails.setCountryCode("PH");
        empDetails.setEmployeeKey("empkey01");
        empDetails.setNumberOfDependents(0);
        
        employmentDetails = new EmploymentDetailsDTO();
        employmentDetails.setDailyRate(3181.82);
        employmentDetails.setMonthlyRate(70000);
        employmentDetails.setHourlyRate(397.73);
        employmentDetails.setJobGradeLevel(1);
        employmentDetails.setJobPositionCode("MGR");
        
        allowances = new AllowancesDTO();
        allowances.setClothingAllowance(1500);
        allowances.setCommunicationAllowance(1799);
        allowances.setGasAllowance(1000);
        allowances.setTravelAllowance(1000);
        employmentDetails.setAllowances(allowances);
        empDetails.setEmploymentDetails(employmentDetails);
        helper.setUp();
	}
	
	@When("I compute employee salary via api resource")
	public void iComputeEmployeeSalaryViaApiResource() throws PayrollModuleException {
		empResponsePayrollDetails = resource.computeEmployeeSalary(empDetails);
	}
	
	@Then("API response is employee details")
	public void ApiResponseIsEmployeeDetails() {
        Assert.assertNotNull(empResponsePayrollDetails);
        Assert.assertTrue(1000 == employmentDetails.getAllowances().getTravelAllowance());
	}
	
}

