package com.cloudsherpas.payroll.api;

import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.cloudsherpas.payroll.dto.core.AllowancesDTO;
import com.cloudsherpas.payroll.dto.core.EmployeeDetailsDTO;
import com.cloudsherpas.payroll.dto.core.EmployeePayrollDeductionsDTO;
import com.cloudsherpas.payroll.dto.core.EmployeePayrollDetailsDTO;
import com.cloudsherpas.payroll.dto.core.EmploymentDetailsDTO;
import com.cloudsherpas.payroll.exception.PayrollModuleException;
import com.cloudsherpas.payroll.service.EmployeePayrollService;
import com.google.appengine.repackaged.com.google.api.client.util.Lists;

@RunWith(MockitoJUnitRunner.class)
public class PayrollResourceTest {

	@Mock
	EmployeePayrollService employeePayrollService;

	EmployeeDetailsDTO empDetails;
	EmployeePayrollDetailsDTO empResponsePayrollDetails;

	@InjectMocks
	PayrollResource payrollResourceApi = new PayrollResource();
	
	AllowancesDTO allowances;
    EmploymentDetailsDTO employmentDetails;

	@Before
	public void setUp() {
		setUpEmployeeDetails();
		setUpEmployeePayrollResponseDetails();
	}

	private void setUpEmployeeDetails() {
		empDetails = new EmployeeDetailsDTO();
		empDetails.setCivilStatus("S");
		empDetails.setCountryCode("PH");
		empDetails.setEmployeeKey("empkey01");
		empDetails.setNumberOfDependents(0);
		
        employmentDetails = new EmploymentDetailsDTO();
        employmentDetails.setDailyRate(3181.82);
        employmentDetails.setMonthlyRate(70000);
        employmentDetails.setHourlyRate(397.73);
        employmentDetails.setJobGradeLevel(1);
        employmentDetails.setJobPositionCode("MGR");
        
        allowances = new AllowancesDTO();
        allowances.setClothingAllowance(1500);
        allowances.setCommunicationAllowance(1799);
        allowances.setGasAllowance(1000);
        allowances.setTravelAllowance(1000);
        employmentDetails.setAllowances(allowances);
        
        empDetails.setEmploymentDetails(employmentDetails);

	}

	private void setUpEmployeePayrollResponseDetails() {
		empResponsePayrollDetails = new EmployeePayrollDetailsDTO();
		empResponsePayrollDetails.setAllowances(allowances);
		empResponsePayrollDetails.setDailyRate(3181.82);
		empResponsePayrollDetails.setMonthlyRate(70000);
		empResponsePayrollDetails.setHourlyRate(397.73);
		EmployeePayrollDeductionsDTO deductions = new EmployeePayrollDeductionsDTO();
		deductions.setAbsencesDeduction(3181.82);
		deductions.setIncomeTaxDeduction(14000);
		deductions.setPagibigDeduction(100);
		deductions.setPhilHealthDeduction(500);
		deductions.setSssDeduction(300);
		empResponsePayrollDetails.setDeductions(deductions);

	}

	@Test
	public void computeEmployeeSalaryTest() throws PayrollModuleException {
		when(employeePayrollService.computeSalaryforEmployee(empDetails))
				.thenReturn(empResponsePayrollDetails);
		EmployeePayrollDetailsDTO result = payrollResourceApi
				.computeEmployeeSalary(empDetails);
		Assert.assertNotNull(result);
		Assert.assertTrue(3181.82 == result.getDailyRate());
		Assert.assertTrue(70000 == result.getMonthlyRate());
		Assert.assertTrue(397.73 == result.getHourlyRate());
		Assert.assertTrue(1000 == result.getAllowances().getGasAllowance());
		Assert.assertTrue(1500 == result.getAllowances().getClothingAllowance());
		Assert.assertTrue(1799 == result.getAllowances().getCommunicationAllowance());
		Assert.assertTrue(1000 == result.getAllowances().getGasAllowance());
		Assert.assertTrue(3181.82 == result.getDeductions()
				.getAbsencesDeduction());
		Assert.assertTrue(14000 == result.getDeductions()
				.getIncomeTaxDeduction());
		Assert.assertTrue(100 == result.getDeductions()
				.getPagibigDeduction());
		Assert.assertTrue(300 == result.getDeductions()
				.getSssDeduction());
	}

	
    @Test
   	public void getAllSalaryTest() throws PayrollModuleException{
    	
    	List<EmployeePayrollDetailsDTO> list = Lists.newArrayList();
    	list.add(empResponsePayrollDetails);
       	when(employeePayrollService.getAllSalary("empKey1"))
           .thenReturn(list);
       	List<EmployeePayrollDetailsDTO> resultList = payrollResourceApi.listEmployeeSalary(("empKey1"));
       	Assert.assertEquals(1, resultList.size());
    }

}
