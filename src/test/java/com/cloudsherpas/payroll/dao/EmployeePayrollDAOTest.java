package com.cloudsherpas.payroll.dao;

import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.cloudsherpas.payroll.dao.impl.EmployeePayrollDAOImpl;
import com.cloudsherpas.payroll.dto.core.EmployeePayrollDeductionsDTO;
import com.cloudsherpas.payroll.dto.core.EmployeePayrollDetailsDTO;
import com.cloudsherpas.payroll.models.EmployeePayrollModel;
import com.cloudsherpas.payroll.models.EmployeeSalaryDeductionModel;
import com.google.appengine.repackaged.com.google.api.client.util.Lists;
import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

@RunWith(MockitoJUnitRunner.class)
public class EmployeePayrollDAOTest {
    @Mock
    EmployeePayrollDAO employeePayrollDAO;

    @InjectMocks
    EmployeePayrollDAOImpl employeePayrollDAOimpl = new EmployeePayrollDAOImpl();

    EmployeePayrollModel empPayrollModel;

    private final LocalServiceTestHelper helper =
            new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig());
    @Before
    public void setUp() {
        setUpEmployeePayrollModel();
    }

    private void setUpEmployeePayrollModel() {
        empPayrollModel = new EmployeePayrollModel();
        empPayrollModel.setDailyRate(2400d);
        EmployeeSalaryDeductionModel deductions = new EmployeeSalaryDeductionModel();
        deductions.setAbsencesDeduction(2000d);
        deductions.setIncomeTaxDeduction(1500d);
        deductions.setPagibigDeduction(100d);
        deductions.setPhilHealthDeduction(500d);
        deductions.setSssDeduction(300d);
        empPayrollModel.setDeductions(deductions);
        empPayrollModel.setEmployeeKey("empKey1");
        empPayrollModel.setGrossSalary(30000d);
        empPayrollModel.setNetSalary(20000d);
        empPayrollModel.setOvertimePay(0d);
        empPayrollModel.setTaxRate(32d);
        empPayrollModel.setId(1234L);
    }

    @Test
    public void getPayrollDetailsByEmployeeTest() {
        List<EmployeePayrollModel> list = Lists.newArrayList();
        list.add(empPayrollModel);
        when(employeePayrollDAO.getPayrollDetailsByEmployee("empKey1"))
                .thenReturn(list);
        List<EmployeePayrollModel> resultList = employeePayrollDAO
                .getPayrollDetailsByEmployee("empKey1");
        Assert.assertEquals(1, resultList.size());
    }

    @Test
    public void getPayrollDetailsByEmployeeNullTest() {
        List<EmployeePayrollModel> list = Lists.newArrayList();
        list.add(empPayrollModel);
        when(employeePayrollDAO.getPayrollDetailsByEmployee("none"))
                .thenReturn(list);
        List<EmployeePayrollModel> resultList = employeePayrollDAO
                .getPayrollDetailsByEmployee("empKey1");
        Assert.assertEquals(0, resultList.size());
    }
}
