package com.cloudsherpas.payroll.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.cloudsherpas.payroll.dao.EmployeePayrollDAO;
import com.cloudsherpas.payroll.dto.core.AllowancesDTO;
import com.cloudsherpas.payroll.dto.core.EmployeeDetailsDTO;
import com.cloudsherpas.payroll.dto.core.EmployeePayrollDeductionsDTO;
import com.cloudsherpas.payroll.dto.core.EmployeePayrollDetailsDTO;
import com.cloudsherpas.payroll.dto.core.EmployeeTimeAndAttendanceDTO;
import com.cloudsherpas.payroll.dto.core.EmploymentDetailsDTO;
import com.cloudsherpas.payroll.dto.core.HolidayPayRulesDTO;
import com.cloudsherpas.payroll.dto.core.OvertimeRulesDTO;
import com.cloudsherpas.payroll.dto.core.WorkingTimeRulesDTO;
import com.cloudsherpas.payroll.enums.HolidayTypeEnum;
import com.cloudsherpas.payroll.enums.OvertimeTypeEnum;
import com.cloudsherpas.payroll.enums.PayRulesMultiplierEnum;
import com.cloudsherpas.payroll.exception.PayrollModuleException;
import com.cloudsherpas.payroll.models.EmployeePayrollModel;
import com.cloudsherpas.payroll.models.EmployeeSalaryDeductionModel;
import com.cloudsherpas.payroll.service.impl.EmployeePayrollServiceImpl;
import com.google.appengine.repackaged.com.google.api.client.util.Lists;

@RunWith(MockitoJUnitRunner.class)
public class EmployeePayrollServiceTest {

	@Mock
	EmployeePayrollRulesService employeePayrollRulesService;

	@Mock
	EmployeePayrollDAO employeePayrollDAO;

	@InjectMocks
	EmployeePayrollService employeePayrollService = new EmployeePayrollServiceImpl();

	EmployeeDetailsDTO empDetails;
	EmployeePayrollDetailsDTO empPayrollDetails;
	EmployeePayrollModel empPayrollModel;
    Map<OvertimeTypeEnum, OvertimeRulesDTO> overtimeRules;
    WorkingTimeRulesDTO workingTimeRules;
    Map<HolidayTypeEnum, HolidayPayRulesDTO> holidayPayRules;
    List<EmployeeTimeAndAttendanceDTO> timeAndAttendanceList;
    EmploymentDetailsDTO employmentDetails;
    AllowancesDTO allowancesDTO;

	@Before
	public void setUp() {
		setUpEmployeeDetails();
		setUpEmployeePayrollDetails();
		setUpEmployeePayrollModel();
		setUpOvertimeRulesDTO();
		setUpWorkingTimeRules();
		setUpHolidayPayRules();
		setUpTimeAndAttendanceList();
	}

	private void setUpEmployeeDetails() {
		empDetails = new EmployeeDetailsDTO();
		empDetails.setEmployeeKey("empKey1");
		empDetails.setCountryCode("PH");
		empDetails.setDateFromStr("07-07-2015");
		empDetails.setDateToStr("08-08-2015");
        employmentDetails = new EmploymentDetailsDTO();
        employmentDetails.setDailyRate(3181.82);
        employmentDetails.setMonthlyRate(70000);
        employmentDetails.setHourlyRate(397.73);
        employmentDetails.setJobGradeLevel(1);
        employmentDetails.setJobPositionCode("MGR");
        
        allowancesDTO = new AllowancesDTO();
        allowancesDTO.setClothingAllowance(1500);
        allowancesDTO.setCommunicationAllowance(1799);
        allowancesDTO.setGasAllowance(1000);
        allowancesDTO.setTravelAllowance(1000);
        employmentDetails.setAllowances(allowancesDTO);
        
        empDetails.setEmploymentDetails(employmentDetails);
		
	}

	private void setUpEmployeePayrollDetails() {
		empPayrollDetails = new EmployeePayrollDetailsDTO();
		empPayrollDetails.setDailyRate(2400d);
		empPayrollDetails.setEmployeeKey("empKey1");
		empPayrollDetails.setDateFromStr("07-07-2015");
		empPayrollDetails.setDateToStr("08-08-2015");
		EmployeePayrollDeductionsDTO deductions = new EmployeePayrollDeductionsDTO();
		deductions.setAbsencesDeduction(0);
		deductions.setIncomeTaxDeduction(0);
		deductions.setPagibigDeduction(0);
		deductions.setPhilHealthDeduction(0);
		deductions.setSssDeduction(0);
		empPayrollDetails.setDeductions(deductions);
	}

	private void setUpEmployeePayrollModel() {
		empPayrollModel = new EmployeePayrollModel();
		empPayrollModel.setDailyRate(2400d);
		empPayrollModel.setEmployeeKey("empKey1");
		empPayrollModel.setOvertimePay(1200d);
		EmployeeSalaryDeductionModel deductions = new EmployeeSalaryDeductionModel();
		deductions.setAbsencesDeduction(2000d);
		deductions.setIncomeTaxDeduction(1500d);
		deductions.setPagibigDeduction(100d);
		deductions.setPhilHealthDeduction(500d);
		deductions.setSssDeduction(300d);
		empPayrollModel.setDeductions(deductions);
	}

	private void setUpOvertimeRulesDTO() {
		overtimeRules = new HashMap<OvertimeTypeEnum, OvertimeRulesDTO>();
		OvertimeRulesDTO dto = new OvertimeRulesDTO();
		dto.setMultiplier(PayRulesMultiplierEnum.HOURLY);
		dto.setOvertimeRate(1.25);
		overtimeRules.put(OvertimeTypeEnum.DAY, dto);
		
		dto = new OvertimeRulesDTO();
		dto.setMultiplier(PayRulesMultiplierEnum.HOURLY);
		dto.setOvertimeRate(1.69);
		overtimeRules.put(OvertimeTypeEnum.SPECIAL_HOLIDAY, dto);
		
		dto = new OvertimeRulesDTO();
		dto.setMultiplier(PayRulesMultiplierEnum.HOURLY);
		dto.setOvertimeRate(2.60);
		overtimeRules.put(OvertimeTypeEnum.REGULAR_HOLIDAY, dto);
		empDetails.setOvertimeRules(overtimeRules);

	}
	
	private void setUpWorkingTimeRules() {
		workingTimeRules = new WorkingTimeRulesDTO();
		workingTimeRules.setDailyMinimumWorkHours(8);
		workingTimeRules.setWeeklyMinimumWorkHours(40);
		empDetails.setWorkingTimeRules(workingTimeRules);
	}
	
	private void setUpTimeAndAttendanceList() {
		timeAndAttendanceList = Lists.newArrayList();
		EmployeeTimeAndAttendanceDTO dto = new EmployeeTimeAndAttendanceDTO();
		dto.setHoliday(true);
		dto.setRenderedHours(2.0);
		dto.setHolidayType(HolidayTypeEnum.REGULAR);
		timeAndAttendanceList.add(dto);
		dto = new EmployeeTimeAndAttendanceDTO();
		dto.setHoliday(false);
		dto.setRenderedHours(10);
		timeAndAttendanceList.add(dto);
		dto = new EmployeeTimeAndAttendanceDTO();
		dto.setHoliday(false);
		dto.setRenderedHours(7);
		dto.setUndertime(60);
		timeAndAttendanceList.add(dto);
		dto = new EmployeeTimeAndAttendanceDTO();
		dto.setHoliday(false);
		dto.setRenderedHours(7.5);
		dto.setTardiness(30);
		timeAndAttendanceList.add(dto);
		dto = new EmployeeTimeAndAttendanceDTO();
		dto.setHoliday(true);
		dto.setHolidayType(HolidayTypeEnum.REGULAR);
		dto.setRenderedHours(10);
		timeAndAttendanceList.add(dto);
		empDetails.setTimeAndAttendanceList(timeAndAttendanceList);
	}
	

	private void setUpHolidayPayRules() {
		holidayPayRules = new HashMap<HolidayTypeEnum, HolidayPayRulesDTO>();
		HolidayPayRulesDTO dto = new HolidayPayRulesDTO();
		dto.setRate(1.30);
		holidayPayRules.put(HolidayTypeEnum.SPECIAL_NON_WORKING, dto);
		dto.setRate(2.0);
		holidayPayRules.put(HolidayTypeEnum.REGULAR, dto);
		empDetails.setHolidayPayRules(holidayPayRules);
	}
	
	@Test
	public void testComputeSalaryForEmployee() throws PayrollModuleException {
		when(employeePayrollRulesService.getEmployeePayrollDetails(empDetails))
				.thenReturn(empPayrollDetails);
		EmployeePayrollDetailsDTO result = employeePayrollService
				.computeSalaryforEmployee(empDetails);
		//verify(employeePayrollRulesService, times(1))
				//.getEmployeePayrollDetails(empDetails);
		System.out.println("holiday" + result.getTotalHolidayPay());
		System.out.println("overtimePay:" + result.getTotalOvertimePay());
		System.out.println(result.getDeductions().getTotalDeduction());
		Assert.assertEquals(27651.366d, result.getNetSalary(), 2);
	}

	@Test
    public void testGetAlLSalary() throws PayrollModuleException {
        List<EmployeePayrollModel> list = Lists.newArrayList();
        list.add(empPayrollModel);
        when(
                employeePayrollDAO.getPayrollDetailsByEmployee(Matchers
                        .anyString())).thenReturn(list);
        List<EmployeePayrollDetailsDTO> result = employeePayrollService
                .getAllSalary(Matchers.anyString());
        Assert.assertEquals(1, result.size());
    }

}
