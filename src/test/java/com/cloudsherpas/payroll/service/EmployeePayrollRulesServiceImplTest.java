package com.cloudsherpas.payroll.service;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.cloudsherpas.payroll.dto.core.AllowancesDTO;
import com.cloudsherpas.payroll.dto.core.EmployeeDetailsDTO;
import com.cloudsherpas.payroll.dto.core.EmployeePayrollDeductionsDTO;
import com.cloudsherpas.payroll.dto.core.EmployeePayrollDetailsDTO;
import com.cloudsherpas.payroll.dto.core.EmploymentDetailsDTO;
import com.cloudsherpas.payroll.exception.PayrollModuleException;
import com.cloudsherpas.payroll.service.impl.EmployeePayrollRulesServiceImpl;

//@Ignore
public class EmployeePayrollRulesServiceImplTest {
    EmployeePayrollRulesServiceImpl employeePayrollService = new EmployeePayrollRulesServiceImpl();
    EmployeeDetailsDTO empDetails;
    EmployeePayrollDeductionsDTO employeeDeductions;
    EmployeePayrollDetailsDTO empPayrollDetails;
    EmploymentDetailsDTO employmentDetails;
    AllowancesDTO allowancesDTO;
    
    @Before
    public void setUpEmployeeDetails() throws PayrollModuleException {
        empDetails = new EmployeeDetailsDTO();
        empDetails.setCivilStatus("S");
        empDetails.setCountryCode("PH");
        empDetails.setEmployeeKey("empkey01");
        empDetails.setNumberOfDependents(0);
        
        employmentDetails = new EmploymentDetailsDTO();
        employmentDetails.setDailyRate(3181.82);
        employmentDetails.setMonthlyRate(70000);
        employmentDetails.setHourlyRate(397.73);
        employmentDetails.setJobGradeLevel(1);
        employmentDetails.setJobPositionCode("MGR");
        
        allowancesDTO = new AllowancesDTO();
        allowancesDTO.setClothingAllowance(1500);
        allowancesDTO.setCommunicationAllowance(1799);
        allowancesDTO.setGasAllowance(1000);
        allowancesDTO.setTravelAllowance(1000);
        employmentDetails.setAllowances(allowancesDTO);
        
        empDetails.setEmploymentDetails(employmentDetails);
        empPayrollDetails = employeePayrollService.getEmployeePayrollDetails(empDetails);
    }
    @Test
    public void testSssDeductionRule() {
        assertTrue(empPayrollDetails.getDeductions().getSssDeduction()==1024.22);
    }
    
    @Test
    public void testPagIbigDeductionRule() {
        assertTrue(empPayrollDetails.getDeductions().getPagibigDeduction()==100);
    }
    
    @Test
    public void testPhilhealthDeductionRule() {
        assertTrue(empPayrollDetails.getDeductions().getPhilHealthDeduction()==1024.00);
    }
    
    public void testTaxRule() {
        assertTrue(empPayrollDetails.getTaxRate()==0.32);
    }
}
