Meta:

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario:  Retrieving employee payroll details
Given Employee Payroll details is saved in the database with the employee key of empKey1
When I retrieving employee payroll details based on employee key empKey1
Then Employee payroll details is being returned