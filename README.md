Payroll Module prototype
==================

Proof of Concept (POC) for Payroll Module that uses Spring and Google Cloud Endpoint

## Authors ##

John Paul Vales <john.vales@cloudsherpas.com>

Cicciolina Miranda <cicciolina.miranda@cloudsherpas.com>

## API Details ##

### a. computeEmployeeSalary ###
This service returns the hourly/daily/monthly rates,gross salary, net salary, salary deductions and allowances of a particular employee.
### Request URL: payroll/compute
### Request Body:
                {
                   "civilStatus": "M",
                   "totalOvertimeHours": 2,
                   "countryCode": "PH",
                   "numberOfAbsences": 1,
                   "jobPositionCode": "MGR",
                   "employeeKey": "1",
                   "numberOfDaysWorked": 10,
                   "numberOfDependents": 1,
                   "jobGradeLevel": 1
                }                
### Response Body:
                 {
                    statusMessage : "Success",
                    statusCode : 200,
                    data : {
                              "employeeKey": "1",
                              "dailyRate": 2272.73,
                              "monthlyRate": 50000,
                              "hourlyRate": 284.09,
                              "grossSalary": 23295.47,
                              "netSalary": 15495.096000000001,
                              "taxRate": 0.2,
                              "overtimePay": 2840.8999999999996,
                              "gasAllowance": 0,
                              "communicationAllowance": 0,
                              "riceAllowance": 1000,
                              "clothingAllowance": 800,
                              "deductions": {
                                   "incomeTaxDeduction": 4659.094,
                                   "sssDeduction": 512.33,
                                   "philHealthDeduction": 256.22,
                                   "pagibigDeduction": 100,
                                   "absencesDeduction": 2272.73,
                                   "totalDeduction": 7800.374000000001
                              }
                           }
                  }
                        

### b. getEmployeeCompensationAndBenefits ###
This api is for getting payroll (compensation and benefit in the form of allowances) information for a specific job position and job grade.
### Request URL: payroll/position
### Request Body:
                {
                    countryCode : "PH",
                    jobPosition : "Manager",
                    jobPositionCode : "MGR",
                    jobGradeLevel : 1
                }
### Response Body:
                 {
                    statusMessage : "Success",
                    statusCode : 200,
                    data : {
                               dailyRate: 3181.82,
                               monthlyRate: 70000,
                               hourlyRate: 397.73,
                               gasAllowance: 1000,
                               communicationAllowance: 1799,
                               riceAllowance: 1000,
                               clothingAllowance: 1500
                           }
                  }
## Drools Rule Engine ##

This application uses Drools as its rule engine. Decision table was used to handle computations of compensation, benefits and necessary deductions based on the supplied data from the api call. See the EMP_COMPENSATION_BENEFITS_RULES_PH.xls for the list of rules.

## Architecture Layers ##
### a. Data Access Object (DAO) Layer ###

This application uses a Data Access Object layer which implements all the persistence logic 
within the app. In this way, instead of having the domain logic communicate directly with 
the database, the domain logic speaks to a DAO layer instead. This DAO layer then communicates 
with the underlying persistence system or service such as Datastore or Cloud SQL.

### b. Service Layer ###

The service layer implements all the business logic in the app. Also if the domain logic needs 
to make atomic transactions in the persistence layer (via DAO calls), the Service layer is the 
one responsible in starting and ending the transaction.

### DAO and Service layer ###

Using DAO and service layer helps us to separate persistence logic from the business logic which 
will reduce coupling in the system. This will help us to easily change a part of the system 
without affecting the other layer.

### c. Resource ###

Resource layer is the entry point for all HTTP requests. This layer is responsible on accepting 
request and serving response.


## Interaction between Layers ##

### Models and Data Transfer Object (DTO) ###

Models and Data Transfer Objects (DTO) are Plain Old Java Objects (POJO). Model is used to 
contain the data that need to be stored in the database such as the entity used for Datastore. 
On the other hand, DTO is used for containing the data for presentation and for encapsulating 
data coming outside from the system (such as converting the payload of a HTTP request to a 
Java object).

Using these two different containers allows us to hide the implementation of the data layer and 
gives us more control on manipulating and changing the structure of data that we serve (either 
request or response) without directly affecting the persistence layer.

## Project Structure ##

### api ###
This package contains all the classes for the Resource layer

### config ###
This package contains all classes related to system configuration such as the Spring beans configuration.

### dao ###
This package contains the abstract class for Data Access Object layer. Each class should focus only on a
single kind of data in the database (e.g. 1 class for each table in database) and each method of
every class should perform a basic transaction in the data (e.g. remove field value of a single 
record in a table).
Also, to reduce redundancy, it is recommended to create a BaseDao class which will contain the
common methods/operations for all kind of data such as saveEntity, deleteEntity, getEntity. This
BaseDao should be a super class of all other DAO classes.

### dao.impl ###
The implementing classes of all abstract class in the dao package. Each abstract class in dao package
could have many implementing classes in this package for different persistent provider. Such as the
CustomerDao abstract class could have DatastoreCustomerDaoImpl for Datastore and SQLCustomerDaoImpl
for Cloud SQL. With this approach, it is easy to deploy the system across environments with different
persistent mechanism.

### dto ###
This package contains all POJO as Data Transfer Object.

### model ###
This package contains all POJO as database entity.

### service ###
This package contains all classes for the Service layer.

### util ###
This package contains all utility classes.

## Spring Configuration ##
This app uses a Spring setup that is intended for the App Engine Platform. See [Optimizing Spring for GAE] 
(https://cloud.google.com/appengine/articles/spring_optimization) for more info. 

### Annotated Bean Configuration ###
Instead of a component based scanning which slows the system, this app explicitly define a lazy loaded beans 
through Annotated Bean Configuration classes in the config package. In this way, beans do not contribute to 
the App Engine instance creation time.

### Autowiring ###
Instead of just using the @Autowiring annotation, additional annotations are added during autowiring to help 
reduce latency during instance creation on App Engine. These are:
 
```java
@Autowired
@Qualifier("customerService")
@Lazy
private CustomerService customerService;
```
## Build Configuration ##
If your are using the Cloud Sherpas nexus, please add this to your settings.xml, inside the mirrors tag:

    <mirror>  
      <id>local-jboss-public</id>  
      <name>Server JBoss Public</name>  
      <mirrorOf>jboss-public-repository-group</mirrorOf>  
      <url>http://104.155.194.213:8081/nexus/content/repositories/jboss-public-releases</url>  
    </mirror>  


## Accessing the API ##

On your local machine, navigate to the project's root directory, and execute the
following maven command:

```
#!bash

mvn appengine:devserver
```

Or by using the *dev_appserver.sh* shell script (included in the google-appengine-sdk)
through the following command:

```
#!bash

dev_appserver.sh target/payroll-1.0-SNAPSHOT
```

The API explorer can be accessed at the following url: [http://localhost:8080/_ah/api/explorer](http://localhost:8080/_ah/api/explorer)




Note: You may need to enable the 'Load unsafe scripts' option on your browser to
properly load the API.